#include <TM4C123.h>
//#include<tm4c123.h>
// or could include
#include<tm4c123gh6pm.h>
void TurnOnLED()
{
	SYSCTL->RCGCGPIO|=0x20;
	while((SYSCTL->RCGCGPIO&0x20)==0){}
	GPIOF->LOCK=0x4C4F434B;
	GPIOF->CR=0x1F;
	GPIOF->DIR=0x0E;
	GPIOF->PUR=0x11;
	GPIOF->DEN=0x1F;
	// 2=RED, 4=BLUE, 8=GREEN
	GPIOF->DATA=0x02;
	while(1)
	{
	}
}
void Timer()
{
	// page 338 enables a timer
	SYSCTL->RCGCTIMER=0x1;
}
int main()
{
	// page 406
	SYSCTL->RCGCGPIO|=0x1; // PORTA
	while((SYSCTL->RCGCGPIO&0x1)==0){}
	//GPIOF->LOCK=0x4C4F434B;
	GPIOA->CR=0x0F;
	// page 663, direction register
	//  0 is input, 1 is output
	GPIOA->DIR=0x0F;
//	GPIOA->PUR=0x11;
	// digital enable register
	//  1 means pin is digitally enabled
	GPIOA->DEN=0x1F;
	//TIMER0->CFG
	GPIOA->DATA=0xF;
	while(1)
	{
	}
}
