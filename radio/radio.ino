const int send_pin=6;
const int receive_pin=7;
void setup()
{
  Serial.begin(9600);
  pinMode(send_pin,OUTPUT);
  pinMode(receive_pin,INPUT);
}
void loop()
{
  static int before=0;
  int value;
  if(before==0)
  {
    digitalWrite(send_pin,HIGH);
    before=1;
  }
  else
  {
    digitalWrite(send_pin,LOW);
    before=0;
  }
  value=digitalRead(receive_pin);
  Serial.println(value);
  delay(100);
}
