const int trigger=8;
const int echo=9;
void setup()
{
  Serial.begin(9600);
  pinMode(trigger,OUTPUT);
  pinMode(echo,INPUT);
}
void loop()
{
  // reset for next trigger
  digitalWrite(trigger,LOW);
  delayMicroseconds(10);
  digitalWrite(trigger,HIGH);
  // a pulse of 10um triggers the pulse
  delayMicroseconds(10);
  digitalWrite(trigger,LOW);
  // read signal from sensor which is a timed line
  //  the HIGH parameter waits until the line goes LOW
  int amount;
  amount=pulseIn(echo,HIGH);
  Serial.println(amount);
  delay(1000);
}
