/*
figured out how to get the motor to spin
 first column is positive voltage
 and second column is ground
+-
13
64
31
46
*/
void setup()
{
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(7,OUTPUT);
  pinMode(LED_BUILTIN,OUTPUT);
}
void loop()
{
  // time to wait between pulses
  const int wait_time=20;
  const int drive_time=5;
  // first cycle
  digitalWrite(4,HIGH);
  delay(drive_time);
  digitalWrite(4,LOW);
  delay(wait_time);
  // second cycle
  digitalWrite(7,HIGH);
  delay(drive_time);
  digitalWrite(7,LOW);
  delay(wait_time);
  // third cycle
  digitalWrite(5,HIGH);
  delay(drive_time);
  digitalWrite(5,LOW);
  delay(wait_time);
  // fourth cycle
  digitalWrite(6,HIGH);
  delay(drive_time);
  digitalWrite(6,LOW);
  delay(wait_time);
}
